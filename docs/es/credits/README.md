---
sidebar: auto
---
# Créditos a donde deben
Queremos agradecer a estas personas por ayudarnos con el manual.

## Backend y desarrollo
* Stefano / superstefano4

## Traducciones
:::tip Las traducciones se están haciendo actualmente 
Debido a que las traducciones necesitan ser aprobadas manualmente. Esta sección ha sido **TEMPORALMENTE** eliminada. Volverá tan pronto como el primer lenguaje alcance el 100% de finalización. Si quieres ayudar, por favor ve a [este](https://translate.pinewood-builders.ga) sitio web. Y presenta un idioma a utilizar. (Contacta conmigo en [este correo electrónico](mailto:stefano@stefanocoding.me), para añadir tu idioma) 
:::


## Correctores
### Jefe de Seguridad
* Csd | Csdi

### PIA
* Omni | TheGreatOmni
### Entrenadores
* TenX
* AnuCat
* CombatSwift
* RogueVader1996
